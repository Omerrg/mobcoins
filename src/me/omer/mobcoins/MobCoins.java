package me.omer.mobcoins;

import me.omer.mobcoins.balance.BalanceManager;
import me.omer.mobcoins.commands.CommandRegistry;
import me.omer.mobcoins.configuration.ConfigManager;
import me.omer.mobcoins.listeners.MobKillListener;
import me.omer.mobcoins.listeners.ShopListener;
import org.bukkit.plugin.java.JavaPlugin;

public class MobCoins extends JavaPlugin {
    private static MobCoins instance;

    public static MobCoins getInstance() {
        return instance;
    }

    private ConfigManager configurationManager;

    @Override
    public void onEnable() {
        instance = this;
        this.configurationManager = new ConfigManager();
        BalanceManager.loadMobData();
        getServer().getPluginManager().registerEvents(new MobKillListener(), this);
        getServer().getPluginManager().registerEvents(new ShopListener(), this);
        new CommandRegistry().registerCommands();
        BalanceManager.loadBalances();
    }

    @Override
    public void onDisable() {
        BalanceManager.saveBalances();
    }


    public ConfigManager getConfigurationManager() {
        return this.configurationManager;
    }
}
