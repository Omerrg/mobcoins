package me.omer.mobcoins.utils;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class ItemStackFactory {
    private ItemStack itemStack;

    public ItemStackFactory() {

    }

    public ItemStackFactory setMaterial(Material material) {
        this.itemStack = new ItemStack(material);
        return this;
    }

    public ItemStackFactory setDisplayName(String displayName) {
        this.itemStack.setItemMeta(new ItemMetaFactory(this.itemStack.getItemMeta()).setDisplayName(displayName).exportItemMeta());
        return this;
    }

    public ItemStack exportItemStack() {
        return this.itemStack;
    }

    public ItemStackFactory setItemStack(ItemStack item) {
        this.itemStack = item.clone();
        return this;
    }

    public ItemStackFactory addLoreLine(String line) {
        this.itemStack.setItemMeta(new ItemMetaFactory(this.itemStack.getItemMeta()).addLoreLine(line).exportItemMeta());
        return this;
    }

    public ItemStackFactory removeLoreLine() {
        this.itemStack.setItemMeta(new ItemMetaFactory(this.itemStack.getItemMeta()).removeLoreLine().exportItemMeta());
        return this;
    }
}

