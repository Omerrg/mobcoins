package me.omer.mobcoins.utils;

import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

class ItemMetaFactory {
    private ItemMeta itemMeta;

    ItemMetaFactory(ItemMeta itemMeta) {
        this.itemMeta = itemMeta;
    }

    ItemMeta exportItemMeta() {
        return this.itemMeta;
    }

    ItemMetaFactory setDisplayName(String name) {
        this.itemMeta.setDisplayName(name);
        return this;
    }

    ItemMetaFactory addLoreLine(String line) {
        List<String> lore;
        if (itemMeta.hasLore())
            lore = this.itemMeta.getLore();
        else
            lore = new ArrayList<>();
        if (lore != null)
            lore.add(line);
        this.itemMeta.setLore(lore);
        return this;
    }

    public ItemMetaFactory removeLoreLine() {
        if(this.itemMeta.hasLore()) {
            List<String> lore = this.itemMeta.getLore();
            lore.remove(lore.size() - 1);
            this.itemMeta.setLore(lore);
        }
        return this;
    }
}
