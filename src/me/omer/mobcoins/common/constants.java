package me.omer.mobcoins.common;

import org.bukkit.ChatColor;


public class constants {
    public static final String PLUGIN_NAME = "MobCoins";
    public static final String CONFIGURATION_FILE_EXTENSION = ".yml";
    public static final String HELP_MESSAGE =
            ChatColor.AQUA + "********************" + ChatColor.WHITE + "MobCoins" + ChatColor.AQUA + "********************\n" +
                    ChatColor.WHITE + "- /mb bal - Display the user balance\n" +
                    ChatColor.WHITE + "- /mb shop - Opens the MobCoins shop\n" +
                    ChatColor.AQUA + "***************************************************";

    public static final String NOT_A_PLAYER_COMMAND_ERROR =
            ChatColor.RED + "I'm sorry, but this command is only for players!";

    public static final String SHOP_TITLE = ChatColor.YELLOW + "MobCoins Store!";

    public static final String SHOW_BALANCE_MESSAGE_FORMAT = ChatColor.AQUA + "Your balance is: %s";

    public static final String BALANCES_FILE_NAME = "balances.csv";

    public static final String DEFAULT_MOB_DATA = "0.05,1";

}
