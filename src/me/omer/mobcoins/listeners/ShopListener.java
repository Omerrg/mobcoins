package me.omer.mobcoins.listeners;

import me.omer.mobcoins.balance.BalanceManager;
import me.omer.mobcoins.common.constants;
import me.omer.mobcoins.utils.ItemStackFactory;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public class ShopListener implements Listener {
    @EventHandler
    public void onShopClick(InventoryClickEvent e) {
        if (!e.getView().getTitle().contains(constants.SHOP_TITLE))
            return;
        e.setCancelled(true);
        if (e.getCurrentItem() != null) {
            if (e.getCurrentItem().getType() != Material.AIR && e.getClickedInventory().getType() == InventoryType.CHEST) {
                ItemStack clickedItem = e.getCurrentItem();
                List<String> lore = clickedItem.getItemMeta().getLore();
                int price = Integer.parseInt(lore.get(lore.size() - 1).split(" ")[1]);
                if (price == -1) {
                    return;
                }

                Player p = (Player) e.getWhoClicked();
                if (BalanceManager.getUserBalance(p.getUniqueId().toString()) >= price) {
                    if (p.getInventory().firstEmpty() == -1) {
                        p.sendMessage(ChatColor.RED + "Please make some room in your inventory.");
                    } else {
                        BalanceManager.subUserBalance(p.getUniqueId().toString(), price);
                        p.getInventory().addItem(new ItemStackFactory().setItemStack(clickedItem).removeLoreLine().exportItemStack());
                        p.sendMessage(ChatColor.GREEN + "Successfully purchased!");
                    }
                }else {
                    p.sendMessage(ChatColor.RED + "Insufficient funds.");

                }
            }
        }
    }
}
