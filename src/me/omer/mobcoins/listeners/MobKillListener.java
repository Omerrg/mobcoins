package me.omer.mobcoins.listeners;

import me.omer.mobcoins.balance.BalanceManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;

import java.util.Random;

public class MobKillListener implements Listener {
    @EventHandler
    public void onMobKill(EntityDeathEvent e) {
        Player killer = e.getEntity().getKiller();
        if (killer == null)
            return;
        double chance = BalanceManager.getMobChance(e.getEntity().getType()) / 100;
        double rng = new Random().nextDouble();
        if (rng <= chance) {
            BalanceManager.addToUserBalance(killer.getUniqueId().toString(), BalanceManager.getMobBounty(e.getEntity().getType()));
        }
    }
}
