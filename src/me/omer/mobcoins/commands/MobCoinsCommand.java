package me.omer.mobcoins.commands;

import me.omer.mobcoins.MobCoins;
import me.omer.mobcoins.balance.BalanceManager;
import me.omer.mobcoins.common.constants;
import me.omer.mobcoins.configuration.ConfigAccess;
import me.omer.mobcoins.utils.ItemStackFactory;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.command.defaults.BukkitCommand;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;
import java.util.HashMap;

public class MobCoinsCommand extends BukkitCommand {
    private static final String[] ALIASES = {"mb", "mobcoins"};
    private static final String DESCRIPTION = "MobCoins main command. Use it to display the help message.";
    private static final String USAGE_MESSAGE = "/mb help";
    private static Inventory shop;
    private static HashMap<ItemStack, Integer> contentToPrice;

    MobCoinsCommand() {
        super(constants.PLUGIN_NAME, DESCRIPTION, USAGE_MESSAGE, Arrays.asList(ALIASES));
        ConfigAccess ca = MobCoins.getInstance().getConfigurationManager().getAccess("shop");
        if (ca.loadedConfiguration.isConfigurationSection("inventory")) {
            ConfigurationSection cs = ca.loadedConfiguration.getConfigurationSection("inventory");
            if (cs != null)
                contentToPrice = getSafeContents(mapFromSection(cs));
        } else {
            contentToPrice = getSafeContents(getDefaultContent());
        }
        ItemStack[] shopContent = generateShopContentFromMap(contentToPrice);
        shop = Bukkit.createInventory(null, shopContent.length % 9 == 0 ? shopContent.length : ((shopContent.length / 9) + 1) * 9, constants.SHOP_TITLE);
        shop.setContents(shopContent);
    }

    private HashMap<ItemStack, Integer> mapFromSection(ConfigurationSection ms) {
        HashMap<ItemStack, Integer> map = new HashMap<>();
        for (String key : ms.getKeys(false)) {
            ConfigurationSection itemSection = ms.getConfigurationSection(key);
            int price = itemSection.getInt("price");
            ItemStack is = (ItemStack) itemSection.get("item");
            map.put(is, price);
        }
        return map;
    }

    private ItemStack[] generateShopContentFromMap(HashMap<ItemStack, Integer> safeContents) {
        ItemStack[] content = new ItemStack[safeContents.size()];
        int i = 0;
        for (ItemStack item : safeContents.keySet()) {
            if(item == null){
                content[i] = new ItemStack(Material.AIR);
            }else  if (item.getType() == Material.AIR) {
                content[i] = item;
            } else {
                content[i] = new ItemStackFactory().setItemStack(item).addLoreLine(ChatColor.GREEN + "Price: " + safeContents.get(item)).exportItemStack();
            }
            i++;
        }
        return content;
    }

    private static HashMap<ItemStack, Integer> getDefaultContent() {
        HashMap<ItemStack, Integer> defaultShopContent = new HashMap<>();
        defaultShopContent.put(new ItemStackFactory().setMaterial(Material.STONE_SWORD).setDisplayName("Please configure the shop!").exportItemStack(), -1);
        return defaultShopContent;
    }

    @SuppressWarnings("unchecked")
    private static HashMap<ItemStack, Integer> getSafeContents(HashMap<ItemStack, Integer> contents) {
        HashMap<ItemStack, Integer> safeContents = (HashMap<ItemStack, Integer>) contents.clone();
        int safeSize = safeContents.size() % 9 == 0 ? safeContents.size() : ((safeContents.size() / 9) + 1) * 9;
        for (int i = safeContents.size(); i < safeSize; i++) {
            safeContents.put(new ItemStack(Material.AIR), -1);
        }
        return safeContents;
    }

    @Override
    public boolean execute(CommandSender commandSender, String label, String[] args) {
        switch (args.length) {
            case 0:
                return displayHelpMessage(commandSender);
            case 1:
                switch (args[0]) {
                    case "bal":
                        return displayBalance(commandSender);
                    case "balance":
                        return displayBalance(commandSender);
                    case "shop":
                        return displayShop(commandSender);
                    default:
                        return displayHelpMessage(commandSender);
                }
        }
        return false;
    }

    private boolean displayHelpMessage(CommandSender sender) {
        sender.sendMessage(constants.HELP_MESSAGE);
        return true;
    }

    private boolean displayBalance(CommandSender sender) {
        if (sender instanceof Player) {
            sender.sendMessage(String.format(constants.SHOW_BALANCE_MESSAGE_FORMAT, ChatColor.YELLOW +
                    String.valueOf(BalanceManager.getUserBalance(((Player) sender).getUniqueId().toString()))));
            return true;
        } else {
            sender.sendMessage(constants.NOT_A_PLAYER_COMMAND_ERROR);
            return false;
        }
    }

    private boolean displayShop(CommandSender sender) {
        if (sender instanceof Player) {
            ((Player) sender).openInventory(shop);
            return true;
        } else {
            sender.sendMessage(constants.NOT_A_PLAYER_COMMAND_ERROR);
            return false;
        }
    }
}
