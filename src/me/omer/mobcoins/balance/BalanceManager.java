package me.omer.mobcoins.balance;

import me.omer.mobcoins.MobCoins;
import me.omer.mobcoins.common.constants;
import me.omer.mobcoins.configuration.ConfigAccess;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class BalanceManager {
    private static HashMap<String, Integer> balances = new HashMap<>();
    private static HashMap<EntityType, Object[]> mobData = new HashMap<>();

    public static void loadBalances() {
        BufferedReader br;
        String line;
        try {
            br = new BufferedReader(new FileReader(new File(MobCoins.getInstance().getDataFolder(), constants.BALANCES_FILE_NAME)));
            while ((line = br.readLine()) != null) {
                String[] lineParts = line.split(",");
                setUserBalance(lineParts[0], Integer.valueOf(lineParts[1]));
            }
            br.close();
        } catch (IOException ignored) {
        }
    }

    public static void saveBalances() {
        List<String[]> dataLines = new ArrayList<>();
        balances.keySet().forEach(key -> dataLines.add(new String[]{key, String.valueOf(balances.get(key))}));
        File csvOutputFile = new File(MobCoins.getInstance().getDataFolder(), constants.BALANCES_FILE_NAME);
        if (!csvOutputFile.exists()) {
            try {
                csvOutputFile.createNewFile();
            } catch (IOException ignored) {
            }
        }
        try (PrintWriter pw = new PrintWriter(csvOutputFile)) {
            dataLines.stream().map(dataLine -> String.join(",", dataLine)).forEach(pw::println);
            pw.flush();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void loadMobData() {
        ConfigAccess ca = MobCoins.getInstance().getConfigurationManager().getAccess("mobs_data", true);
        for (EntityType type : EntityType.values()) {
            if (type == EntityType.UNKNOWN)
                continue;
            if (LivingEntity.class.isAssignableFrom(type.getEntityClass())) {
                if (ca.get(type.name()) == null) {
                    ca.set(type.name(), constants.DEFAULT_MOB_DATA);
                }
                String mobValue = String.valueOf(ca.get(type.name()));
                mobData.put(type, Arrays.stream(mobValue.split(",")).map(Double::valueOf).toArray());
            }
        }
    }

    public static int getUserBalance(String uuid) {
        if (!balances.containsKey(uuid))
            balances.put(uuid, 0);
        return balances.get(uuid);
    }

    private static void setUserBalance(String uuid, int balance) {
        balances.put(uuid, balance);
    }

    public static void addToUserBalance(String uuid, int amount) {
        setUserBalance(uuid, getUserBalance(uuid) + amount);
    }

    public static double getMobChance(EntityType type) {
        return (Double) mobData.get(type)[0];
    }

    public static int getMobBounty(EntityType type) {
        return ((Double) mobData.get(type)[1]).intValue();
    }

    public static void subUserBalance(String uuid, int amount) {
        setUserBalance(uuid, getUserBalance(uuid) - amount);
    }
}
